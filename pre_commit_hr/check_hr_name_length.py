import yaml
import argparse
from typing import Sequence

errors: list = []


def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('--hr-name-length', default='38', required=False, type=int)
    parser.add_argument('filenames', nargs='*', help='Filenames to check.')
    args = parser.parse_args(argv)

    for filename in args.filenames:
        try:
            _validate_file(filename, args)
        except Exception as ex:
            _collect_errors({"source": filename, "message": f"{type(ex).__name__} {ex.args}"})
    if len(errors) > 0:
        _print_errors()
        return 1
    else:
        return 0


def _validate_file(fileToValidate, args):
    with open(fileToValidate) as f:
        for definition in yaml.load_all(f, Loader=yaml.SafeLoader):
            if (
                    not definition
                    or "kind" not in definition
                    or definition["kind"] != "HelmRelease"
            ):
                continue

            name = definition["metadata"]["name"]
            if len(name) > args.hr_name_length:
                _collect_errors(
                    {
                        "message": f"hr name {name} is {len(name)} symbols.",
                        "recommendation": f"Use {name[:args.hr_name_length]} instead or another."
                    }
                )
                continue


def _collect_errors(error):
    errors.append(error)


def _print_errors():
    for i in errors:
        print(f"[ERROR] {i['message']}: {i['recommendation']}")


if __name__ == "__main__":
    raise SystemExit(main())
